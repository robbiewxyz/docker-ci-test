# build
FROM mhart/alpine-node:base as test-build
COPY file1 /
RUN node --version > /file2
RUN cat /file1 /file2 > /file3

# use
FROM mhart/alpine-node:base as test-use
COPY --from=test-build /file3 /
RUN cat /file3
